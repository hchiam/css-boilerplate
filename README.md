# My personal CSS boilerplate

![version](https://img.shields.io/github/release/hchiam/css-boilerplate) [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://github.com/hchiam/css-boilerplate/blob/master/LICENSE)

## Example usage

```html
<link
  rel="stylesheet"
  href="https://cdn.jsdelivr.net/gh/hchiam/css-boilerplate@6.0.0/style.css"
  integrity="sha384-thHsntRltFQFgrHgl7/avBhQ+uXkABJlP8wgGW62xGYYlLgaPGbEFQ0xdPcVDDI8"
  crossorigin="anonymous"
/>
```

```bash
bash get-integrity.sh
```
